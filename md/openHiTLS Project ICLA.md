### **The Project of openHiTLS Individual Contributor License Agreement ("Agreement")**

**By signing this contributor license agreement, I understand and agree that this project and contributions to it are public and that a record of the contribution (including all personal information I submit with it, including my name and email address) is maintained indefinitely and may be redistributed consistent with this project, compliance with the open source license(s) involved, and maintenance of authorship attribution.**

Thank you for your interest in the openHiTLS Project ("openHiTLS" or the "Project") incubated and hosted by the Huawei Technologies Co., Ltd ( "Huawei"). Huawei is on behalf of the Project, and is also the legal entity that is making this Agreement with you.

---

In order to clarify the rights granted with Contributions from any person or entity, the Project must have a Contributor License Agreement (the "Agreement" or "CLA") on file that has been signed by each Contributor, indicating agreement to the license terms below. This Agreement is for Your protection as a Contributor as well as the protection of the Project and its users; it does not change Your rights to use Your own Contributions for any other purpose.

This version of the Agreement allows an individual to submit Contributions to the Project, and to grant copyright and patent licenses to Huawei.

Please read this CLA carefully before completing and signing it, and keep a copy for Your records.

The rights that You grant to us under these terms are effective on the date You first submitted a Contribution to openHiTLS, even if Your submission took place before the date You agreed to these terms and conditions. Except for the license granted herein to Huawei and recipients of the Work of the Project, You reserve all rights, title and interest in and to Your Contributions.

1. Definitions.
   
   "Project" refers to the openHiTLS Project incubated and hosted by Huawei, which may be accessed at the address as listed in Appendix A.
   
   "Contributor"or "You" (or "Your") shall mean the individual copyright owner or individual authorized by the copyright owner that is making this agreement with the Huawei, and voluntarily submits a Contribution to the Project.
   
   "Contribution" shall mean the code, documentation, or any original work of authorship, including a modification of or addition to an existing work, that is intentionally submitted by You for inclusion in the work of the Project (the "Work"). For the purposes of this definition, "submitted" means any form of electronic, verbal, or written communication sent to the Project or its representatives, including but not limited to communication on electronic mailing lists, source code control systems, and issue tracking systems that are managed by, or on behalf of, the Project for the purpose of discussing and improving the Work, but excluding communication that is conspicuously marked or otherwise designated in writing by You as "Not a Contribution.".
2. Contributor Grant of Copyright License.
   
   Subject to the terms and conditions of this Agreement, You hereby grant to the Project and to recipients of the Work a perpetual, non-exclusive, worldwide, no-charge, royalty-free, irrevocable copyright license to reproduce, prepare derivative works of, publicly display, publicly perform, sublicense, and distribute Your Contribution and derivative works thereof.
3. Contributor Grant of Patent License
   
   Subject to the terms and conditions of this Agreement, You hereby grant to the Project and to recipients of the Work, a perpetual, non-exclusive, worldwide, no-charge, royalty-free, irrevocable (except as stated in this section) patent license to make, have made, use, offer to sell, sell, import, and otherwise transfer "the Work"; provided, however, that such license applies only to those patent claims licensable by You that are necessarily infringed by Your Contribution(s) alone or by combination of Your Contribution(s) with the Work to which such Contribution(s) were submitted.
   
   If any entity institutes patent litigation against You or any other entity (including a cross-claim or counterclaim in a lawsuit) alleging that You or such other entity has directly or contributorily infringed a patent as a result of some act of making, having made, using, offering to sell, selling, or importing Your Contribution, or the combination of Your Contribution with the Work to which You submitted "the Contribution", then any patent license granted by You under this CLA to the entity instituting patent litigation shall terminate as of the date such litigation is filed.
   
   Apart from the licenses granted in section 2 & 3, You reserve all right, title and interest in and to Your Contribution.
4. You represent that You are legally entitled to grant the licenses in section 2 and 3. If Your employer (s) has rights to Your Contribution, You represent that You have received permission to submit Your Contribution on behalf of that employer, that Your employer has waived such rights for Your Contribution to "the Project", or that Your employer has executed a separate Corporate Contributor License Agreement with Huawei.
5. You represent that Your Contribution is Your original work (see section 7 for submissions on behalf of others). You represent that Your submission of Your Contribution includes complete details of any third-party license or other restriction (including, but not limited to, related copyrights, patents and trademarks) of which You are personally aware and which are associated with any part of Your Contribution.
6. You are not expected to provide support for Your "Contributions", except to the extent You desire to provide support. You may provide support for free, for a fee, or not at all. Unless required by applicable law or agreed to in writing and except as expressly set forth in this CLA, You provide Your Contribution on an "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, either express or implied, including, without limitation, any warranties or conditions of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A PARTICULAR PURPOSE.
7. Should You wish to submit work that is not Your original work, You may submit it to the Project separately from Your Contribution, identifying the complete details of its origin and of any license or other restriction (including, but not limited to, related copyrights, patents and trademarks) of which You are aware, and conspicuously marking the work to show "it has been submitted on behalf of a third party" and the name of that party.
8. You agree to notify the Huawei of any facts or circumstances of which You become aware that would make these representations inaccurate in any respect in time.

**Appendix A: Address of the openHiTLS Project**

**[1].** [**https://gitcode.com/openhitls/openhitls**](https://gitcode.com/openhitls/openhitls)

<style>
  @counter-style lower-alpha-rparen {
    system: extends lower-alpha;
    suffix: ') ';
  }
  h3 {
    text-align: center;
  }
</style>
